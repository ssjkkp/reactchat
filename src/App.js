import React, { useState, useRef, useEffect } from 'react';
import './App.css';

//FIREBASE SDK
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

//FIREBASE HOOKS
import {useAuthState} from 'react-firebase-hooks/auth';
import {useCollectionData} from 'react-firebase-hooks/firestore';
//import { geolocated } from "react-geolocated";
//import { getLocationByLatLng } from "./request";
import axios from 'axios';

firebase.initializeApp({

})

const auth=firebase.auth();
const firestore=firebase.firestore();



function App() {
//signed in-> user is an object
//signed out-> user is null
  const [user] =useAuthState(auth);
  const [notes, setNotes] = useState([]) 
  //location
  // if (navigator.geolocation) {
  //   navigator.geolocation.getCurrentPosition(getPosition);
  // }
  // function getPosition(position) {
  //   console.log(position.coords.latitude, position.coords.longitude);
  // }
const counrtryName='';
const countryCode='';
 
    axios.get('https://ipapi.co/json/').then((response) => {
        let data = response.data;
        // this.setState({
            console.log(data);
            // console.log(data.country_calling_code);
        //});
    }).catch((error) => {
      console.log("error");
        console.log(error);
    });

    //useEffect(() => {
  //     fetch('https://extreme-ip-lookup.com/json/')
  //     .then( res => res.json())
  //     .then(response => {
  //      console.log("Country is : ", response);
  //    })
  //    .catch((data, status) => {
  //      console.log('Request failed:', data);
  //    });
  //  },[])

  return (
    <div className="App">
      <header>

      </header>

      <section>
        {user? <ChatRoom/>:<SignIn/>}
      </section>
    </div>
  );
}


function SignIn(){
  const signInWithGoogle=()=>{
    const provider =new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  }
return(
  <button onClick={signInWithGoogle}>Sign in with Google</button>
)
}

function SignOut(){
  return auth.currentUser && (
    <button className="sign-out" onClick={()=>auth.signOut()}>Sign Out</button>
  )
}

function ChatRoom(){

  const dummy=useRef();

  const messagesRef=firestore.collection('messages');
  const query=messagesRef.orderBy('createdAt').limit(25);
  //console.log(messagesRef);
  //listen to data with hook
  //react will react to changes and update the view
  const[messages]=useCollectionData(query,{idField:'id'});

  const [formValue,setFormValue] = useState('');

  const sendMessage=async(e)=>{

    //prevents page refresh
    e.preventDefault();
    const{uid,photoURL}=auth.currentUser;

    //adds new message in Firebase
    await messagesRef.add({
      text:formValue,
      createdAt:firebase.firestore.FieldValue.serverTimestamp(),
      uid,
      photoURL
    })

    setFormValue('');
    //dummy for the automated scroll down after message
    dummy.current.scrollIntoView({ behavior: 'smooth'});
  }

  return(
    <>
      <main>
        {messages && messages.map(msg=><ChatMessage key={msg.id} message={msg}/>)}
      
        {/* Add this dummy to get screen to scroll down after new message */}
        <span ref={dummy}></span>
      </main>
      <form onSubmit={sendMessage}>
        {/* bind state formValue to form input */}
        <input value={formValue} onChange={(e)=>setFormValue(e.target.value)}/>

        <button type="sumbit"  disabled={!formValue}>&gt;</button>

      </form>
    </>
  )
}

function ChatMessage(props){
  const{text,uid, photoURL}=props.message;

  const messageClass=uid===auth.currentUser.uid?'sent':'received';
  return (<>
  <div className={`message ${messageClass}`}>
    <img src={photoURL} />
    <p>
      {text}
    </p>
    </div>
    </>
  )
}
// export default geolocated({
//   positionOptions:{
//     enableHighAccuracy:false
//   },
//   userDecisionTimeout:10000
// })(App);
export default App;
